import { useState, useEffect } from "react";
import axios from "axios";

export const useFetch = (url, id) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      axios
        .get(url)
        .then(res => {
          //return specific airport
          let data = res.data;
          if (id) {
            data = data.filter(x => x.airportCode === id)[0];
          }
          setData(data);
        })
        .catch(err => {
          console.log(err);
        });
      setLoading(false);
    };
    fetchData();
  }, [url, id]);

  return { data, loading };
};
