import React from "react";
import { HeartSpinner } from "react-spinners-kit";
import { Link } from "react-router-dom";
import { useFetch } from "utils/fetchUtil";
import { FixedSizeList as List } from "react-window";
import AutoSizer from "react-virtualized-auto-sizer";

//components
import AirportHeader from "./Header";
import AirportItem from "./Item";

const AirportList = () => {
  const { data, loading } = useFetch(
    "https://api.qantas.com/flight/refData/airport"
  );
  let airports = data ? data : [];

  //airport list item
  const Row = ({ index, style }) => (
    <Link to={{ pathname: "/airport/" + airports[index].airportCode }}>
      <AirportItem
        name={airports[index].airportName}
        country={airports[index].country.countryName}
        num={index}
        style={style}
      />
    </Link>
  );

  let content = (
    <div className="loader">
      <HeartSpinner size={50} color="#e40000" />
    </div>
  );
  if (!loading) {
    content = (
      <React.Fragment>
        <AirportHeader />
        <AutoSizer>
          {({ height, width }) => (
            <List
              height={height - 50}
              itemCount={airports.length}
              itemSize={60}
              width={width}
            >
              {Row}
            </List>
          )}
        </AutoSizer>
      </React.Fragment>
    );
  }

  return content;
};

export default AirportList;
