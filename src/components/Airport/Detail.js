import React from "react";
import { HeartSpinner } from "react-spinners-kit";
import { Link } from "react-router-dom";
import { useFetch } from "utils/fetchUtil";
import { useParams } from "react-router-dom";

//icons
import { IoIosAirplane } from "react-icons/io";
import { IoIosCheckmarkCircleOutline } from "react-icons/io";
import { IoIosCloseCircleOutline } from "react-icons/io";

import styles from "./Detail.module.css";

const AirportDetail = () => {
  let { id } = useParams();
  const { data, loading } = useFetch(
    "https://api.qantas.com/flight/refData/airport",
    id
  );
  let airport = data ? data : null;

  //array of features
  let features = [
    { name: "Domestic", value: "domesticAirport" },
    { name: "International", value: "internationalAirport" },
    { name: "Regional", value: "regionalAirport" },
    { name: "eTicket", value: "eticketableAirport" }
  ];

  let content = (
    <div className="loader">
      <HeartSpinner size={50} color="#e40000" />
    </div>
  );
  if (!loading && airport) {
    content = (
      <div className={styles.detail}>
        <div className="container">
          <div className={styles.header}>
            <div>
              <h1 data-testid="named" className={styles.name}>
                {airport.airportName}
              </h1>
              <h4 data-testid="country" className={styles.country}>
                {airport.country.countryName}
              </h4>
              <p data-testid="timezone" className={styles.timezone}>
                {airport.city.timeZoneName}
              </p>
            </div>
            <IoIosAirplane style={{ fontSize: "64px", color: "#fff" }} />
          </div>
          <div className={styles.features}>
            {features.map(x => (
              <div key={x.value} className={styles.feature}>
                <div
                  className={`${styles.icon} ${
                    airport[x.value] ? styles.featYes : styles.featNo
                  }`}
                >
                  {airport[x.value] ? (
                    <IoIosCheckmarkCircleOutline />
                  ) : (
                    <IoIosCloseCircleOutline />
                  )}
                </div>
                {x.name}
              </div>
            ))}
          </div>
          <Link to="/">
            <div className={styles.btn}>
              <button>Back to Airports</button>
            </div>
          </Link>
        </div>
      </div>
    );
  }

  return content;
};

export default AirportDetail;
