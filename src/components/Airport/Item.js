import React from "react";
import PropTypes from "prop-types";
import { IoIosArrowForward } from "react-icons/io";

import styles from "./Item.module.css";

const AirportItem = ({ name, country, num, style }) => (
  <div
    style={style}
    className={`${styles.item} ${num % 2 ? styles.odd : styles.even}`}
  >
    <div>
      <h4 data-testid="name" className={styles.name}>
        {name}
      </h4>
      <p data-testid="country" className={styles.country}>
        {country}
      </p>
    </div>
    <div>
      <IoIosArrowForward />
    </div>
  </div>
);

AirportItem.propTypes = {
  name: PropTypes.string,
  country: PropTypes.string,
  num: PropTypes.number,
  style: PropTypes.object
};

export default AirportItem;
