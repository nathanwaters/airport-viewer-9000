import React from "react";
import styles from "./Header.module.css";

const AirportHeader = () => {
  return (
    <header>
      <h4 className={styles.header}>Global Airport List</h4>
    </header>
  );
};

export default AirportHeader;
