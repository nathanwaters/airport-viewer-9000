import React from "react";
import { render, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import AirportItem from "../Item";

afterEach(cleanup);

it("renders without crashing", () => {
  render(<AirportItem />);
});

it("renders correctly", () => {
  const { getByTestId } = render(
    <AirportItem name="Airport" country="Country" />
  );
  expect(getByTestId("name")).toHaveTextContent("Airport");
  expect(getByTestId("country")).toHaveTextContent("Country");
});
