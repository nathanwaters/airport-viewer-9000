import React from "react";
import { render, cleanup } from "@testing-library/react";
import { renderHook } from "@testing-library/react-hooks";
import "@testing-library/jest-dom/extend-expect";
import { useFetch } from "utils/fetchUtil";
import AirportList from "../List";

afterEach(cleanup);
jest.setTimeout(15000);

it("renders without crashing", () => {
  render(<AirportList />);
});

it("successfully loads data from API", async () => {
  const { result, waitForNextUpdate } = renderHook(() =>
    useFetch("https://api.qantas.com/flight/refData/airport")
  );
  await waitForNextUpdate();
  expect(result.current.data.length).toBeGreaterThan(0);
  expect(result.current.data[0].airportCode).not.toBeNull();
});
