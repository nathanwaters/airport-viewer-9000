import React from "react";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import { renderHook } from "@testing-library/react-hooks";
import "@testing-library/jest-dom/extend-expect";
import { useFetch } from "utils/fetchUtil";
import AirportDetail from "../Detail";

jest.setTimeout(15000);

it("renders without crashing", () => {
  render(
    <MemoryRouter initialEntries={["/airport/AAA"]}>
      <AirportDetail />
    </MemoryRouter>
  );
});

it("successfully loads data from API", async () => {
  const { result, waitForNextUpdate } = renderHook(() =>
    useFetch("https://api.qantas.com/flight/refData/airport", "AAA")
  );
  await waitForNextUpdate();
  expect(result.current.data.airportCode).toBe("AAA");
});
