import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";

//components
import AiportList from "components/Airport/List";
import AirportDetail from "components/Airport/Detail";

import "normalize.css";
import "./App.css";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route path="/airport/:id">
          <AirportDetail />
        </Route>
        <Route path="/airports">
          <AiportList />
        </Route>
        <Redirect from="/" to="/airports" />
      </Switch>
    </Router>
  );
};

export default App;
